package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.client.rtu.ModbusRtuClientMessage;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuBody;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuHeader;

public class ModbusRtuForDtuClientTestMessage extends ModbusRtuClientMessage {

    public ModbusRtuForDtuClientTestMessage(byte[] message) {
        super(message);
    }

    public ModbusRtuForDtuClientTestMessage(ModbusRtuHeader head) {
        super(head);
    }

    public ModbusRtuForDtuClientTestMessage(ModbusRtuHeader head, ModbusRtuBody body) {
        super(head, body);
    }

}
