package com.iteaj.iot.redis.proxy;

import com.iteaj.iot.handle.proxy.ProtocolHandleInvocationHandler;
import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;
import com.iteaj.iot.handle.proxy.ProtocolHandleProxyMatcher;
import com.iteaj.iot.redis.ComplexRedisProducer;
import com.iteaj.iot.redis.SimpleRedisProducer;
import com.iteaj.iot.redis.producer.RedisProducer;

public class RedisProxyMatcher implements ProtocolHandleProxyMatcher {

    @Override
    public boolean matcher(Object target) {
        return target instanceof RedisProducer;
    }

    @Override
    public ProtocolHandleInvocationHandler invocationHandler(Object target) {
        return new ProtocolHandleInvocationHandler(target) {

            @Override
            protected Class<? extends ProtocolHandleProxy> getProxyClass() {
                return RedisProducer.class;
            }

            @Override
            protected Object proxyHandle(Object value, Object proxy) {
                if(getTarget() instanceof SimpleRedisProducer) {
                    getTarget().persistence(value);
                } else if(getTarget() instanceof ComplexRedisProducer) {

                }

                return value;
            }

            @Override
            public RedisProducer getTarget() {
                return (RedisProducer) super.getTarget();
            }
        };
    }
}
