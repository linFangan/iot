package com.iteaj.iot.client.component;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.ClientMessage;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.buffer.ByteBuf;

public abstract class TcpClientComponent<M extends ClientMessage> extends SocketClientComponent<M, ByteBuf> {

    public TcpClientComponent() { }

    /**
     * @param config 默认客户端配置
     */
    public TcpClientComponent(ClientConnectProperties config) {
        super(config);
    }

    public TcpClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    public abstract TcpSocketClient createNewClient(ClientConnectProperties config);
}
