package com.iteaj.iot.server;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.business.BusinessFactory;

public class ServerProtocolHandleFactory extends BusinessFactory<ServerProtocolHandle> {

    @Override
    protected Class<? extends Protocol> getProtocolClass(ServerProtocolHandle item) {
        return item.protocolClass();
    }

    @Override
    protected Class<ServerProtocolHandle> getServiceClass() {
        return ServerProtocolHandle.class;
    }
}
